#include <stdio.h>
#include <string.h>

#define ARRSIZE 50
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 


int main()
{
	unsigned char arr[ARRSIZE] = { 0 };
	FILE *file = fopen("wambo.bin", "rb");
	fread(arr, ARRSIZE, 1, file);
	for (int i = 0; i < arr[i] != NULL; i++)
	{
		printf(BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(arr[i]));
	}

}